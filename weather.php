<?php
session_start();
include './db.php';

if (!isset($_SESSION['username'])) {
    header('Location: ./login.php');
    exit();
}
$username = $_SESSION['username'];
$stmt = $pdo->prepare("SELECT * FROM users WHERE username = ?");
$stmt->execute([$username]);
$user = $stmt->fetch();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./styles.css">
    <title>Weather App</title>
</head>
<body>
    <div class="city">
        <label for="city">City:</label><br>
        <input name="city" type="text" id="city"></input><br>
        <button onclick="getWeather()">Hae</button>
    </div>
    <div id="image"></div>
    <div id="root" class="divv"></div>
    <a class="aa" href="./logout.php">Logout</a>
    <script src="./script.js"></script>
    <script>document.getElementById('city').value = "<?php if ($user['city'] != "") { echo $user['city'];};?>"</script>
</body>
</html>