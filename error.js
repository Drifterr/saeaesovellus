const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const error = urlParams.get('error')
document.getElementById('error-message').innerHTML = error;