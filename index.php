<!-- index.php -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Weather</title>
    <link rel="stylesheet" href="./styles.css">
</head>
<body>
    <p>Have an account? <a href="./login.php">Login</a></p>
    <p>Don't have an account? <a href="./register.php">Register</a></p>
</body>
</html>
