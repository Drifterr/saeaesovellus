<?php
session_start();
include './db.php';

// Check if form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Check if username and password are provided
    if (!empty($_POST['username']) && !empty($_POST['password'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];

        // Prepare a SQL statement to fetch the user with the provided username
        $stmt = $pdo->prepare("SELECT * FROM users WHERE username = ?");
        $stmt->execute([$username]);
        $user = $stmt->fetch();

        if ($user && password_verify($password, $user['password'])) {
            // Authentication successful, set session variables
            $_SESSION['username'] = $user['username'];
            // Redirect to the dashboard or another page
            header('Location: ./weather.php');
            exit();
        } else {
            // Authentication failed, redirect back to login page with error message
            header('Location: ./login.php?error=Invalid%20username%20or%20password');
            exit();
        }
    } else {
        // Username or password not provided, redirect back to login page with error message
        header('Location: ./login.php?error=Please%20enter%20username%20and%20password');
        exit();
    }
}
?>


<!-- login.php -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="./styles.css">
</head>
<body>
    <div class="login-container">
        <h2>Login</h2>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="username">Username:</label>
            <input type="text" id="username" name="username" required><br>
            <label for="password">Password:</label>
            <input type="password" id="password" name="password" required><br>
            <button type="submit">Login</button>
        </form>
        <p id="error-message"></p>
        <p>Don't have an account? <a href="./register.php">Register</a></p>
    </div>
    <script src="./error.js"></script>
</body>
</html>
