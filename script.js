async function getWeather() {
    let div = document.getElementById("root")
    let ii = document.getElementById("image")
    const city = document.getElementById("city").value;
    if (city.length > 0) {
        const cityCoords = await fetch(`https://api.openweathermap.org/geo/1.0/direct?q=${city}&limit=1&appid=5d737ef2174072d3199bc4ed9f4b095d`);
        const cityData = await cityCoords.json();
        const weatherData = await fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${cityData[0].lat}&lon=${cityData[0].lon}&appid=5d737ef2174072d3199bc4ed9f4b095d&units=metric`)
        const wea = await weatherData.json();
        let Sunrise = new Date(wea.sys.sunrise*1000);
        let Sunset = new Date(wea.sys.sunset*1000);
        let sunrise = Sunrise.toLocaleTimeString();
        let sunset = Sunset.toLocaleTimeString();
        ii.innerHTML = `<img src="https://openweathermap.org/img/wn/${wea.weather[0].icon}@4x.png">`
        div.innerHTML = "Sunrise: " + "<span class='data'>" + sunrise + "</span>" + " Sunset: " + "<span class='data'>" + sunset + "</span>" + "<br>" + "It is " + "<span class='data'>" + wea.main.temp + "°C" + "</span>" + ", but it feels like " + "<span class='data'>" + wea.main.feels_like + "°C" + "</span>" + " outside in " + "<span class='data'>" + wea.name + "</span>" + "<br>" + "Weather: " + "<span class='data'>" + wea.weather[0].main + "</span>" + ", " + "<span class='data'>" + wea.weather[0].description + "</span>" + "<br>" + "The lowest today is " + "<span class='data'>" + wea.main.temp_min + "°C" + "</span>" + " and highest is " + "<span class='data'>"  + wea.main.temp_max + "°C" + "</span>" + "<br>" + "The visibility is " + "<span class='data'>" + wea.visibility +  " meters" + "</span>" + "<br>" + "The wind speed is " + "<span class='data'>" + wea.wind.speed + " m/s" + "</span>"
    } else {
        ii.innerHTML = "";
        div.innerHTML = "Error retrieving weather. Please check spelling."
    }
    
}