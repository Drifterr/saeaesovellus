<?php
session_start();
include './db.php';

// Check if form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Check if username, password, and confirm password are provided
    if (!empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['confirm_password'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $confirm_password = $_POST['confirm_password'];
        $city = $_POST['city'];

        // Check if passwords match
        if ($password !== $confirm_password) {
            header('Location: ./register.php?error=Passwords%20do%20not%20match');
            exit();
        }

        // Check if username is already taken
        $stmt = $pdo->prepare("SELECT * FROM users WHERE username = ?");
        $stmt->execute([$username]);
        if ($stmt->fetch()) {
            header('Location: ./register.php?error=Username%20already%20taken');
            exit();
        }

        // Hash the password before storing it in the database
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);

        // Insert the new user into the database
        $stmt = $pdo->prepare("INSERT INTO users (username, password, city) VALUES (?, ?, ?)");
        $stmt->execute([$username, $hashed_password, $city]);

        // Redirect to login page after successful registration
        header('Location: ./login.php?success=Registration%20successful.%20You%20can%20now%20login.');
        exit();
    } else {
        // Username, password, or confirm password not provided
        header('Location: ./register.php?error=Please%20fill%20in%20all%20fields');
        exit();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="./styles.css">
</head>
<body>
    <div class="register-container">
        <h2>Register</h2>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="username">Username:</label>
            <input type="text" id="username" name="username" required><br>
            <label for="city">City:</label>
            <input type="text" id="city" name="city"><br>
            <label for="password">Password:</label>
            <input type="password" id="password" name="password" required><br>
            <label for="confirm_password">Confirm Password:</label>
            <input type="password" id="confirm_password" name="confirm_password" required><br>
            <button type="submit">Register</button>
        </form>
        <p id="error-message"></p>
        <p>Already have an account? <a href="./login.php">Login</a></p>
    </div>
    <script src="./error.js"></script>
</body>
</html>
